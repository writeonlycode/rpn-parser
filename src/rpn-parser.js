export function parser(input) {
  const inputTokenized = lexer(input);
  const stack = [];

  for (const token of inputTokenized) {
    switch (token.typeOf()) {
      case "PropositionSymbol":
        stack.push(token);
        break;
      
      case 'ConjunctionSymbol':
        const conjunctionFirstChild = stack.pop()

        if ( !conjunctionFirstChild )
          throw new SyntaxError("Conjunction expects two formulas, but it has been given none.");

        token.children.unshift(conjunctionFirstChild);

        const conjunctionSecondChild = stack.pop()

        if ( !conjunctionSecondChild )
          throw new SyntaxError("Conjunction expects two formulas, but it has been given only one.");

        token.children.unshift(conjunctionSecondChild);

        stack.push(token);
        break;

      case 'DisjunctionSymbol':
        const disjunctionFirstChild = stack.pop()

        if ( !disjunctionFirstChild )
          throw new SyntaxError("Disjunction expects two formulas, but it has been given none.");

        token.children.unshift(disjunctionFirstChild);

        const disjunctionSecondChild = stack.pop()

        if ( !disjunctionSecondChild )
          throw new SyntaxError("Disjunction expects two formulas, but it has been given only one.");

        token.children.unshift(disjunctionSecondChild);

        stack.push(token);
        break;

      case 'ConditionalSymbol':
        const conditionalFirstChild = stack.pop()

        if ( !conditionalFirstChild )
          throw new SyntaxError("Conditional expects two formulas, but it has been given none.");

        token.children.unshift(conditionalFirstChild);

        const conditionalSecondChild = stack.pop()

        if ( !conditionalSecondChild )
          throw new SyntaxError("Conditional expects two formulas, but it has been given only one.");

        token.children.unshift(conditionalSecondChild);

        stack.push(token);
        break;

      case 'BiconditionalSymbol':
        const biconditionalFirstChild = stack.pop()

        if ( !biconditionalFirstChild )
          throw new SyntaxError("Biconditional expects two formulas, but it has been given none.");

        token.children.unshift(biconditionalFirstChild);

        const biconditionalSecondChild = stack.pop()

        if ( !biconditionalSecondChild )
          throw new SyntaxError("Biconditional expects two formulas, but it has been given only one.");

        token.children.unshift(biconditionalSecondChild);

        stack.push(token);
        break;


      case 'NegationSymbol':
        const negationChild = stack.pop()

        if ( !negationChild )
          throw new SyntaxError("Negation expects two one formula, but it has been given none.");

        token.children.unshift(negationChild);

        stack.push(token);
        break;

      default:
        throw new SyntaxError("Ops... Something went wrong...");
    }

  }

  if ( stack.length > 1 )
    throw new SyntaxError(`The formula must have only one root, but it has ${stack.length}.`);

  return stack[0];
}

function lexer(input) {
  const inputTokenized = [];

  if (typeof input !== "string") {
    return;
  }

  for (let c of input) {
    if (PropositonSymbol.isMatch(c)) {
      inputTokenized.push(new PropositonSymbol(c));
    } else if (NegationSymbol.isMatch(c)) {
      inputTokenized.push(new NegationSymbol(c));
    } else if (ConjunctionSymbol.isMatch(c)) {
      inputTokenized.push(new ConjunctionSymbol(c));
    } else if (DisjunctionSymbol.isMatch(c)) {
      inputTokenized.push(new DisjunctionSymbol(c));
    } else if (ConditionalSymbol.isMatch(c)) {
      inputTokenized.push(new ConditionalSymbol(c));
    } else if (BiconditionalSymbol.isMatch(c)) {
      inputTokenized.push(new BiconditionalSymbol(c));
    } else if (c === " ") {
      // skip if it's a space
    } else {
      throw new SyntaxError(`The symbol "${c}" is not part of the lexicon.`);
    }
  }

  return inputTokenized;
}

class PropositonSymbol {
  constructor(symbol) {
    this.symbol = symbol;
  }

  toString() {
    return "[PropositionSymbol]";
  }

  typeOf() {
    return "PropositionSymbol";
  }

  static symbols = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];

  static isMatch(input) {
    if (this.symbols.includes(input)){
      return true;
    } else {
      return false;
    }
  }
}

class NegationSymbol {
  constructor(symbol) {
    this.symbol = symbol;
    this.children = []
  }

  toString() {
    return "[NegationSymbol]";
  }

  typeOf() {
    return "NegationSymbol";
  }

  static symbols = ["N"]

  static isMatch(input) {
    if (this.symbols.includes(input)){
      return true;
    } else {
      return false;
    }
  }
}

class ConjunctionSymbol {
  constructor(symbol) {
    this.symbol = symbol;
    // this.parent = null;
    this.children = []
  }

  toString() {
    return "[ConjunctionSymbol]";
  }

  typeOf() {
    return "ConjunctionSymbol";
  }

  static symbols = ["K"]

  static isMatch(input) {
    if (this.symbols.includes(input)){
      return true;
    } else {
      return false;
    }
  }
}

class DisjunctionSymbol {
  constructor(symbol) {
    this.symbol = symbol;
    this.children = []
  }

  toString() {
    return "[DisjunctionSymbol]";
  }

  typeOf() {
    return "DisjunctionSymbol";
  }

  static symbols = ["A"]

  static isMatch(input) {
    if (this.symbols.includes(input)){
      return true;
    } else {
      return false;
    }
  }
}

class ConditionalSymbol {
  constructor(symbol) {
    this.symbol = symbol;
    this.children = []
  }

  toString() {
    return "[ConditionalSymbol]";
  }

  typeOf() {
    return "ConditionalSymbol";
  }

  static symbols = ["C"]

  static isMatch(input) {
    if (this.symbols.includes(input)){
      return true;
    } else {
      return false;
    }
  }
}

class BiconditionalSymbol {
  constructor(symbol) {
    this.symbol = symbol;
    this.children = []
  }

  toString() {
    return "[BiconditionalSymbol]";
  }

  typeOf() {
    return "BiconditionalSymbol";
  }

  static symbols = ["E"]

  static isMatch(input) {
    if (this.symbols.includes(input)){
      return true;
    } else {
      return false;
    }
  }
}

class SyntaxError extends Error {
  constructor(message) {
    super(message);
    this.name = 'Syntax Error';
  }
}
