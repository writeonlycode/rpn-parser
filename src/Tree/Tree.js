import React from "react";
import Node from "./Node";
import "./Tree.scss";

const Tree = ({ data }) => {
  return (
    <div className="Tree">
      <Node data={data} key={`component-${Math.random().toString(16).slice(2)}`} />
    </div>
  );
};

export default Tree;
